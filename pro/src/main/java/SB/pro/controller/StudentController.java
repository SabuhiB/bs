package SB.pro.controller;

import SB.pro.dto.StudentDto;
import SB.pro.repository.StudentRepository;
import SB.pro.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/{id}")
    public void getStudent(@PathVariable int id){
        studentService.getStudent(id);


    }
}
