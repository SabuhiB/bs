package SB.pro.dto;

import lombok.Data;

@Data
public class StudentDto {
    private String name;
    private String lastName;
    private String phone;
    private Long age;
}
